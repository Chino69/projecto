import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import * as firebase from 'firebase';
import { Platform } from '@ionic/angular';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  user: any;
  success: boolean;
  constructor(public router: Router, private afAuth: AngularFireAuth, private platform: Platform,) {
    this.user = this.afAuth.authState;
    /*if(this.user){
      console.log('TRANSPORTE sirvio')
      this.router.navigate(['/home']);
    }*/
   }

  ngOnInit() {
  }
  
  async webGoogleLogin(): Promise<void> {
    try {
      const provider = new firebase.auth.GoogleAuthProvider();
      const credential = await this.afAuth.auth.signInWithPopup(provider);
      
      if(this.user){
        this.success = true;
      }
      if(this.success){
        this.router.navigate(['/form']);
      }
      console.log(this.success)
    } catch(err) {
      console.log(err)

      const authError = err.code;

      
    }
  }
 

  async webFbLogin(): Promise<void>{
  try {
    const provider = new firebase.auth.FacebookAuthProvider();
    const credential = await this.afAuth.auth.signInWithPopup(provider);
    console.log(credential);
    console.log(this.user);
    if(this.user){
      this.success = true;
    }
    if(this.success){
      this.router.navigate(['/form']);
    }
    console.log(this.success)
  } catch(err) {
    console.log(err)

    const authError = err.code;

    
  }
}
}
