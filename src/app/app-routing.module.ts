import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'user', loadChildren: './user/user.module#UserPageModule' },
  { path: 'form', loadChildren: './form/form.module#FormPageModule' },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'company-list', loadChildren: './company-list/company-list.module#CompanyListPageModule' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
